import json
import os
import unittest
import yaml

from internationalization.Config import init_logging


def get_test_config(*args, **kwargs):
    with open('tests/test-config.yml', 'r') as config_file:
        return yaml.safe_load(config_file)


init_logging()
test_config = get_test_config()


class MockResponse:
    def __init__(self, status_code=200, json_content=None):
        self.content = json.dumps(json_content)
        self.text = self.content
        self.status_code = status_code

    def json(self):
        return json.loads(self.content)

    def content(self):
        return self.content


def build_wikidata_result(translations):
    json_content = {
        "results": {
            "bindings": [
            ]
        }
    }
    for lng in translations:
        json_content["results"]["bindings"].append({
            "item": {
                "type": "uri",
                "value": "http://www.wikidata.org/entity/Q12345"
            },
            "label": {
                "type": "literal",
                "value": translations[lng],
                "xml:lang": lng
            },
            "ordinal": {
                "datatype": "http://www.w3.org/2001/XMLSchema#int",
                "type": "literal",
                "value": "0"
            }
        })
    return MockResponse(status_code=200, json_content=json_content)


class BaseTest(unittest.TestCase):

    def setUp(self):
        sidre_db = test_config["SIDRE"]["sidre-sqlite-database"]
        wikidata_db = test_config["TRANSLATION"]["wikidata-search-cache-sqlite-database"]
        if os.path.exists(sidre_db):
            os.remove(sidre_db)
        if os.path.exists(wikidata_db):
            os.remove(wikidata_db)
