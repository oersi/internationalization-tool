import unittest
from unittest import mock

from internationalization.Translator import Translator
from tests.BaseTest import BaseTest, MockResponse, build_wikidata_result, get_test_config


def mocked_requests_get_test_translate(*args, **kwargs):
    if args[0] == 'https://query.wikidata.org/bigdata/namespace/wdq/sparql':
        if "test term" in kwargs["params"]["query"]:
            return build_wikidata_result({
                "de": "übersetzter test Begriff",
                "en": "translated test term"
            })
        elif "test 1" in kwargs["params"]["query"]:
            return build_wikidata_result({"en": "test 1 en"})
        elif "test 2" in kwargs["params"]["query"]:
            return build_wikidata_result({"en": "test 2 en"})
        elif "test 3" in kwargs["params"]["query"]:
            return build_wikidata_result({"en": "test 3 en"})
        elif "missed term" in kwargs["params"]["query"]:
            return build_wikidata_result({})
        elif "wikidata bad request" in kwargs["params"]["query"]:
            return MockResponse(status_code=400)
        elif "wikidata forbidden" in kwargs["params"]["query"]:
            return MockResponse(status_code=403)
    return MockResponse(status_code=404)


class TranslatorTest(BaseTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get_test_translate)
    @mock.patch('internationalization.Config.get_config', side_effect=get_test_config)
    def test_translate_term(self, mock_get, mock_config):
        result = Translator().translate("test term", ["de", "en"])
        self.assertDictEqual(result, {"de": "übersetzter test Begriff", "en": "translated test term"})

    @mock.patch('requests.get', side_effect=mocked_requests_get_test_translate)
    @mock.patch('internationalization.Config.get_config', side_effect=get_test_config)
    def test_translate_term_with_separators(self, mock_get, mock_config):
        result = Translator().translate("test 1, test 2 / test 3", ["en"])
        self.assertDictEqual(result, {"en": "test 1 en, test 2 en / test 3 en"})

    @mock.patch('requests.get', side_effect=mocked_requests_get_test_translate)
    @mock.patch('internationalization.Config.get_config', side_effect=get_test_config)
    def test_translate_term_without_result_from_wikidata(self, mock_get, mock_config):
        result = Translator().translate("missed term", ["en"])
        self.assertDictEqual(result, {"en": ""})

    @mock.patch('requests.get', side_effect=mocked_requests_get_test_translate)
    @mock.patch('internationalization.Config.get_config', side_effect=get_test_config)
    def test_translate_bad_request_from_wikidata(self, mock_get, mock_config):
        result = Translator().translate("wikidata bad request", ["en"])
        self.assertDictEqual(result, {"en": ""})

    @mock.patch('requests.get', side_effect=mocked_requests_get_test_translate)
    @mock.patch('internationalization.Config.get_config', side_effect=get_test_config)
    def test_translate_forbidden_from_wikidata(self, mock_get, mock_config):
        self.assertRaises(PermissionError, lambda: Translator().translate("wikidata forbidden", ["en"]))


if __name__ == '__main__':
    unittest.main()
