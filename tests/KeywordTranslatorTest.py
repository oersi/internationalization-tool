import unittest
from unittest import mock

from internationalization.sidre.KeywordRepository import KeywordRepository
from internationalization.sidre.KeywordTranslator import KeywordLoader, translate_sidre_keywords
from tests.BaseTest import BaseTest, MockResponse, build_wikidata_result, get_test_config


test_config = get_test_config()
sidre_url = test_config["SIDRE"]["search-api-url"]


def mocked_requests_sidre_post_request_invalid_char(*args, **kwargs):
    if args[0] == sidre_url:
        return MockResponse(status_code=200, json_content={
            "aggregations": {
                "keywords": {
                    "buckets": [
                        {"key": {
                            "keyword": "test term\\[W0]\\"
                        }}
                    ]
                }
            }
        })
    return MockResponse(status_code=404)


def mocked_requests_post_test_translate_keywords(*args, **kwargs):
    if args[0] == sidre_url:
        return MockResponse(status_code=200, json_content={
            "aggregations": {
                "keywords": {
                    "buckets": [
                        {"key": {
                            "keyword": "test term"
                        }}
                    ]
                }
            }
        })
    return MockResponse(status_code=404)


def mocked_requests_get_test_translate_keywords(*args, **kwargs):
    if args[0] == 'https://query.wikidata.org/bigdata/namespace/wdq/sparql':
        return build_wikidata_result({"en": "translated test term", "da": "+"})
    return MockResponse(status_code=404)


class KeywordLoaderTest(BaseTest):

    @mock.patch('requests.post', side_effect=mocked_requests_sidre_post_request_invalid_char)
    @mock.patch('internationalization.Config.get_config', side_effect=get_test_config)
    def test_load_keyword_with_invalid_chars(self, mock_post, mock_config):
        keywords = KeywordLoader().load_next()
        self.assertCountEqual(keywords, ["test term"])


class KeywordTranslatorTest(BaseTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get_test_translate_keywords)
    @mock.patch('requests.post', side_effect=mocked_requests_post_test_translate_keywords)
    @mock.patch('internationalization.Config.get_config', side_effect=get_test_config)
    def test_translate_keywords(self, mock_get, mock_post, mock_config):
        translate_sidre_keywords()
        translations = KeywordRepository().retrieve_all_translations()
        self.assertCountEqual(translations, ["test term, translated test term"])


if __name__ == '__main__':
    unittest.main()
