import unittest
from unittest import mock

from internationalization.wikidata.WikidataService import WikidataService
from tests.BaseTest import BaseTest, MockResponse, get_test_config


def mocked_requests_get_bad_request(*args, **kwargs):
    if args[0] == 'https://query.wikidata.org/bigdata/namespace/wdq/sparql':
        return MockResponse(status_code=400)
    return MockResponse(status_code=404)


def mocked_requests_get_forbidden_request(*args, **kwargs):
    if args[0] == 'https://query.wikidata.org/bigdata/namespace/wdq/sparql':
        return MockResponse(status_code=403)
    return MockResponse(status_code=404)


class WikidataServiceTest(BaseTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get_bad_request)
    @mock.patch('internationalization.Config.get_config', side_effect=get_test_config)
    def test_invalid_request(self, mock_get, mock_config):
        self.assertRaises(IOError, lambda: WikidataService().process_search("invalid search term"))

    @mock.patch('requests.get', side_effect=mocked_requests_get_forbidden_request)
    @mock.patch('internationalization.Config.get_config', side_effect=get_test_config)
    def test_forbidden_access(self, mock_get, mock_config):
        self.assertRaises(PermissionError, lambda: WikidataService().process_search("test search"))


if __name__ == '__main__':
    unittest.main()
