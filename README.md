# Internationalization Tool

With the help of this tool translations of arbitrary terms (in arbitrary languages) into other languages can be determined. For this purpose, a search is made in [Wikidata](https://www.wikidata.org) and the labels from Wikidata are used as translations.

The tool can be used in the context of an SIDRE instance and can be set up automatically via SIDRE-setup (for example, translations are created for the OERSI keywords or for labels of controlled vocabularies). But the tool can also be used in any other scenario to determine translations via Wikidata.

## Configuration

Default configurations should be set in the _config/config.yml_ file.

To be able to access wikidata, it is necessary to set your own values for `GENERAL.client-identifier`, `GENERAL.client-version`, `GENERAL.client-contact-information` to fulfill the [Wikidata API Etiquette](https://www.mediawiki.org/wiki/API:Etiquette).

### Logging

Log-configuration can be adjusted in [logging.conf](config/logging.conf). See also https://docs.python.org/3/library/logging.html

### TRANSLATION

Configurations for the general translation-process

* Wikidata-queries are slow and the result does not change very often - because of this, the results of the queries are stored in an internal cache (sqlite3-db). This makes queries faster, if executed again and prevents unnecessary traffic on wikidata.
    * `wikidata-search-cache-sqlite-database` - the name of the sqlite3-database that is used for the cache
    * `cache_expiration_time_in_ms` - expiration time of every item in the cache-data
* Search-terms are prepared (split up) per default before the Wikidata-search is executed. The reason for this is that often search terms are used, which contain several, comma-separated single terms or also /-separated alternative terms. As a rule, you get better results / translations from Wikidata if you search directly for the single terms.
    * `search-term-preparation_multiple-term-separator_default` - regex to split incoming search-terms into separate searches and process a search for every part of the resulting list -> will be combined afterwards by joining via ", "
    * `search-term-preparation_alternative-term-separator_default` - regex to split incoming search-terms into alternative searches for the same term and process a search for every part of the resulting list -> will be combined afterwards by joining via " / "
    * If you want to use your own, custom preparation-process, you need to pass in your own implementation of a `SearchTermPreparer` while using `internationalization.Translator.Translator(search_term_preparer=<YOUR_IMPLEMENTATION>)`
* In Wikidata there are some Wikidata-Types that are not very interesting for most of the translation-scenarios (like [scientific journal](https://www.wikidata.org/wiki/Q5633421), [award](https://www.wikidata.org/wiki/Q618779) or [Unicode character](https://www.wikidata.org/wiki/Q29654788)). You can exclude these types in the wikidata search:
    * `wikidata-exclusions_default` - default-list of such types that should be excluded for all searches, if no other exclusions are given
    * You can also set the list of types that should be excluded while creating a new instance of `internationalization.Translator.Translator(wikidata_exclusions=<YOUR_LIST>)`

### Search Index

This tool can be used to translate values from the search index via Wikidata and make them available for the search index as a synonyms file for Elasticsearch.

Configurations for search-index-specific tasks

* `output_languages` - the ISO-639-1-Codes of the target-languages that should be used in your search-index instance
* `search-api-url` - URL of the search-API of your search-index-instance
* Values in the search index sometimes contain special-characters that break the Wikidata-search or cause unwanted results. Sometimes they are completely invalid. For this purpose, you can modify/skip the keywords from your search-index-instance before using them for the Wikidata-search.
    * `keyword-substitutions` - substitute a (sub-)string of the keyword via a regex and replace-value
    * `keyword-skip-values` - list of keywords that shouldn't be processed -> skip search for these keywords
    * `valid_keyword_translation_regex` - translated keywords must match this regex, otherwise they will be skipped. If omitted: keep all translations
* During the search-index-processes like loading data from the external providers, the translation-process should not block any of these processes. Therefore, the regular and automated translation process should run independently and asynchronously from the other search-index processes. Since the initial translation process as well as the process after the expiration of the cache can run for a long time (several hours), this process is also divided again into two subprocesses. One process determines the search-index values, translates them and writes them to a sqlite3 database. A second process reads all current translations from the sqlite3 database and creates a synonym file for elasticsearch. This way, an up-to-date status in the synonym file can be created at any time, even if not all translations have been determined yet.
    * `sidre-sqlite-database` - name of the database for search-index-specific persistence

## Reuse translation tool

1. You can use the tool directly via command-line to translate a single term
   * prerequisites: python3 and pip3 needs to be installed
   * init:
      ```
      git clone https://gitlab.com/oersi/sidre/internationalization-tool.git
      cd internationalization-tool
      pip3 install -r requirements.txt
      ```
   * translate:
     ```
     python3 -m internationalization.Translator "randomization" de,es,fr
     ```
2. You can also implement your own Python-scripts and use the classes from `internationalization.Translator` directly for your translations.

## Examples

* Simple translation via Command-Line of the term _randomization_ into German, Spanish and French:
  ```
  internationalization-tool$ python3 -m internationalization.Translator "randomization" de,es,fr
  ```
* Translate Search Index keywords and create synonyms file from translated keywords
  ```
  internationalization-tool$ python3 -m internationalization.sidre.KeywordTranslator
  ```
  ```
  internationalization-tool$ python3 -m internationalization.sidre.KeywordSynonymGenerator /my/path/synonyms.txt
  ```

## Tests

Execute command from root-directory

* Run tests `python3 -m unittest tests/**Test.py`
* Run tests with test-coverage `coverage3 run -m unittest tests/**Test.py`
