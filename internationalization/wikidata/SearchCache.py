import abc
from typing import Optional


class SearchCache(abc.ABC):
    @abc.abstractmethod
    def init(self) -> None:
        """Initialize the cache"""

    @abc.abstractmethod
    def put(self, identifier, query_config, wikidata_result: list) -> None:
        """Put the given result from wikidata to the cache"""

    @abc.abstractmethod
    def get(self, identifier, query_config) -> Optional[list]:
        """Get the wikidata result from the cache; None, if non-existent in the cache"""
