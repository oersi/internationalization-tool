import json
import sqlite3
from datetime import datetime, timedelta, timezone
from typing import Optional

from .. import Config
from ..wikidata.SearchCache import SearchCache


def transform_query_config(query_config):
    return json.dumps(query_config, sort_keys=True, indent=2, ensure_ascii=False) if query_config else ""


class SqliteSearchCache(SearchCache):
    def __init__(self):
        super().__init__()
        self.config = Config.get_config()

    def get_db(self):
        return self.config["TRANSLATION"]["wikidata-search-cache-sqlite-database"]

    def init(self) -> None:
        con = sqlite3.connect(self.get_db())
        cursor = con.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS wikidata_search_result (id INTEGER PRIMARY KEY AUTOINCREMENT, term text, query_config text, last_update_time_utc text, json_result text)")
        cursor.execute("CREATE UNIQUE INDEX IF NOT EXISTS term_index ON wikidata_search_result(term, query_config)")
        con.commit()
        con.close()

    def put(self, identifier, query_config, wikidata_result: list) -> None:
        json_query_config = transform_query_config(query_config)
        json_result = json.dumps(wikidata_result, sort_keys=True, indent=2, ensure_ascii=False)
        con = sqlite3.connect(self.get_db())
        cursor = con.cursor()
        cursor.execute("DELETE FROM wikidata_search_result WHERE term=? AND query_config=?", (identifier, json_query_config))
        cursor.execute("INSERT INTO wikidata_search_result (term, query_config, json_result, last_update_time_utc) VALUES (?, ?, ?, ?)", (identifier, json_query_config, json_result, datetime.now(timezone.utc)))
        con.commit()
        con.close()

    def get(self, identifier, query_config) -> Optional[list]:
        json_query_config = transform_query_config(query_config)
        con = sqlite3.connect(self.get_db())
        cursor = con.cursor()
        result = None
        cache_expiration_time_in_ms = self.config["TRANSLATION"]["cache_expiration_time_in_ms"]
        if cache_expiration_time_in_ms:
            cache_expiration_bound = datetime.now(timezone.utc) - timedelta(milliseconds=cache_expiration_time_in_ms)
            rows = cursor.execute("SELECT json_result FROM wikidata_search_result WHERE term=? AND query_config=? AND last_update_time_utc>?", (identifier, json_query_config, cache_expiration_bound))
        else:
            rows = cursor.execute("SELECT json_result FROM wikidata_search_result WHERE term=? AND query_config=?", (identifier, json_query_config))

        for row in rows:
            result = json.loads(row[0])
            break
        con.close()
        return result
