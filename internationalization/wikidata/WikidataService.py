import logging
import requests

from .. import Config
from ..wikidata.SqliteSearchCache import SqliteSearchCache

default_sparql_endpoint = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql'
query_prefixes = '''
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wikibase: <http://wikiba.se/ontology#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX bd: <http://www.bigdata.com/rdf#>
'''


class WikidataService:
    def __init__(self):
        super().__init__()
        self.config = Config.get_config()
        self.assure_valid_config()
        self.user_agent = self.config["GENERAL"]["client-identifier"] + "/" + str(self.config["GENERAL"]["client-version"]) + " (" + self.config["GENERAL"]["client-contact-information"] + ") SIDRE-Internationalization-Tool/0.1"
        self.cache = SqliteSearchCache()
        self.cache.init()

    def assure_valid_config(self):
        general = self.config["GENERAL"]
        if not general["client-identifier"] or not general["client-version"] or not general["client-contact-information"]:
            raise ValueError("client properties in general-configuration has to be set!")

    def process_search(self, term, exclusions=[], url=default_sparql_endpoint, api_language="de"):
        cached_result = self.cache.get(term, [url, api_language, exclusions])
        if cached_result is not None:
            logging.debug("use cached result")
            return cached_result
        exclusions_filter = "\n".join(map(lambda e: "FILTER NOT EXISTS { ?item wdt:P31|wdt:P279 wd:%s. }" % e, exclusions))
        query = '''
        %s
    
        SELECT DISTINCT ?item ?label ?ordinal
        WHERE
        {
          SERVICE wikibase:mwapi
          {
            bd:serviceParam wikibase:endpoint "www.wikidata.org";
                            wikibase:api "EntitySearch";
                            mwapi:search "%s";
                            mwapi:limit 500;
                            mwapi:language "%s".
            bd:serviceParam wikibase:limit 500 .
            ?item wikibase:apiOutputItem mwapi:item.
            ?ordinal wikibase:apiOrdinal true.
          }
          ?item rdfs:label ?label.
          %s
        } ORDER BY ASC(?ordinal) LIMIT 500
        ''' % (query_prefixes, term, api_language, exclusions_filter)
        headers = {'User-Agent': self.user_agent}
        resp = requests.get(url, headers=headers, params={'query': query, 'format': 'json'})
        if resp.status_code == 403:
            raise PermissionError("Forbidden to access wikidata - did you set your correct client-configuration?")
        elif resp.status_code != 200:
            raise IOError("Could not process wikidata-search for '" + term + "'")
        result = resp.json()["results"]["bindings"]
        self.cache.put(term, [url, api_language, exclusions], result)
        return result
