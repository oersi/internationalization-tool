import logging
import re
import requests

from .KeywordRepository import KeywordRepository
from .. import Config
from ..Translator import Translator


def translate_sidre_keywords():
    logging.info("Start translation of search index keywords")
    config = Config.get_config()
    output_languages = config["SIDRE"]["output_languages"]
    valid_keyword_translation_regex = config["SIDRE"]["valid_keyword_translation_regex"] if "valid_keyword_translation_regex" in config["SIDRE"] else None

    def keyword_translation_filter(x):
        if x is None:
            return False
        return re.match(valid_keyword_translation_regex, x) if valid_keyword_translation_regex is not None else True

    keyword_repository = KeywordRepository()
    keyword_loader = KeywordLoader()
    translator = Translator()
    keywords = keyword_loader.load_next()
    while keywords is not None:
        logging.debug("Processing next " + str(len(keywords)) + " keywords")
        for keyword in keywords:
            translations = list(translator.translate(keyword, output_languages).values())
            translations.append(keyword)
            translations = sorted(set(filter(keyword_translation_filter, map(lambda x: x.lower(), translations))))
            if len(translations) > 1:
                keyword_repository.create_or_update(keyword, translations)
        keywords = keyword_loader.load_next()
    translator.log_stats()
    logging.info("Translation of search index keywords done")


class KeywordLoader:
    def __init__(self):
        config = Config.get_config()
        self.search_api_url = config["SIDRE"]["search-api-url"]
        self.keyword_fieldname = config["SIDRE"]["keyword-fieldname"]
        self.substitutions = config["SIDRE"]["keyword-substitutions"]
        self.skip_values = config["SIDRE"]["keyword-skip-values"]
        self.user_agent = config["GENERAL"]["client-identifier"] + "/" + str(config["GENERAL"]["client-version"]) + " (" + config["GENERAL"]["client-contact-information"] + ") SIDRE-Internationalization-Tool/0.1"
        self.loaded = False
        self.after_key = None

    def substitute(self, keyword):
        substituted_keyword = keyword
        for sub in self.substitutions:
            substituted_keyword = re.sub(sub["regex"], sub["value"], substituted_keyword)
        return substituted_keyword

    def prepare(self, keywords):
        prepared_keywords = []
        for k in keywords:
            item_words = re.split("[,/]", self.substitute(k))
            prepared_keywords.extend(list(map(lambda a: a.strip(), item_words)))
        return prepared_keywords

    def load_next(self):
        if self.loaded:
            return None
        headers = {'Content-type': 'application/json', 'Accept': 'application/json', 'User-Agent': self.user_agent}
        data = {
            "size": 0,
            "aggregations": {
                "keywords": {
                    "composite": {
                        "size": 2000,
                        "sources": [
                            {"keyword": {"terms": {"field": self.keyword_fieldname}}}
                        ]
                    }
                }
            }
        }
        if self.after_key:
            data["aggregations"]["keywords"]["composite"]["after"] = self.after_key
        result = requests.post(self.search_api_url, headers=headers, json=data)
        json_result = result.json()

        if "after_key" in json_result["aggregations"]["keywords"]:
            self.after_key = json_result["aggregations"]["keywords"]["after_key"]
        else:
            self.loaded = True

        return sorted(set(
            filter(
                lambda a: a not in self.skip_values,
                self.prepare([b["key"]["keyword"] for b in json_result["aggregations"]["keywords"]["buckets"]])
            )
        ))


if __name__ == "__main__":
    Config.init_logging()
    translate_sidre_keywords()
