import sqlite3
from datetime import datetime, timezone

from .. import Config


class KeywordRepository:
    def __init__(self):
        self.config = Config.get_config()
        self.init()

    def get_db(self):
        return self.config["SIDRE"]["sidre-sqlite-database"]

    def init(self) -> None:
        con = sqlite3.connect(self.get_db())
        cursor = con.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS keyword_translation (id INTEGER PRIMARY KEY AUTOINCREMENT, keyword text, translation text, last_update_time_utc text)")
        cursor.execute("CREATE INDEX IF NOT EXISTS keyword_index ON keyword_translation(keyword)")
        con.commit()
        con.close()

    def create_or_update(self, keyword, translations):
        con = sqlite3.connect(self.get_db())
        cursor = con.cursor()
        cursor.execute("DELETE FROM keyword_translation WHERE keyword=?", (keyword,))
        for translation in translations:
            cursor.execute("INSERT INTO keyword_translation (keyword, translation, last_update_time_utc) VALUES (?, ?, ?)", (keyword, translation, datetime.now(timezone.utc)))
        con.commit()
        con.close()

    def retrieve_all_translations(self):
        con = sqlite3.connect(self.get_db())
        con.row_factory = lambda c, row: row[0]
        cursor = con.cursor()
        translations = cursor.execute("select distinct group_concat(translation, ', ') from keyword_translation group by keyword").fetchall()
        con.commit()
        con.close()
        return translations
