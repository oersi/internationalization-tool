import logging
import os
import sys
import tempfile

from .KeywordRepository import KeywordRepository
from .. import Config


def create_synonyms_file(file):
    keyword_repository = KeywordRepository()
    translations = keyword_repository.retrieve_all_translations()
    dirname, basename = os.path.split(file)
    with tempfile.NamedTemporaryFile(mode="w", prefix=basename, dir=dirname, delete=False) as tmp_file:
        logging.debug("Writing temporary synonyms file " + tmp_file.name)
        for synonyms in translations:
            tmp_file.write(synonyms + "\n")
        tmp_file.flush()
        os.rename(tmp_file.name, file)
    logging.info("Created synonyms file " + file)


if __name__ == "__main__":
    Config.init_logging()
    create_synonyms_file(sys.argv[1])
