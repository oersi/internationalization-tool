import yaml
import logging
import logging.config


def get_config():
    with open('config/config.yml', 'r') as config_file:
        try:
            return yaml.safe_load(config_file)
        except yaml.YAMLError:
            logging.exception("Could not load config-file")
            return {}


def init_logging():
    logging.config.fileConfig('config/logging.conf', disable_existing_loggers=False)
