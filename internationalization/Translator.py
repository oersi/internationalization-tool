import datetime
import logging
import re
import sys

from . import Config
from .wikidata.WikidataService import WikidataService


class Stats:
    start_time = datetime.datetime.now()
    entries = 0
    language_stats = {}

    def elapsed_time(self):
        return datetime.datetime.now() - self.start_time

    def add_entry(self):
        self.entries += 1

    def add_hit(self, language):
        if language in self.language_stats:
            self.language_stats[language] += 1
        else:
            self.language_stats[language] = 1

    def add_miss(self, language):
        if language not in self.language_stats:
            self.language_stats[language] = 0


class SearchTermPreparer:
    def __init__(self, multiple_term_separator, alternative_term_separator):
        self.multiple_term_separator = multiple_term_separator
        self.alternative_term_separator = alternative_term_separator

    def prepare(self, term):
        multiple_terms = re.split(self.multiple_term_separator, term)
        return {
            "originalSearchTerm": term,
            "multiTerm":
                list(map(
                    lambda a: {"altTerm": list(map(lambda t: {"term": t}, a))},
                    filter(
                        lambda a: a,
                        map(
                            lambda t: list(filter(None, map(lambda a: a.strip() if a.strip() else False, re.split(self.alternative_term_separator, t)))),
                            multiple_terms
                        )
                    )
                ))
        }


class Translator:
    def __init__(self, wikidata_exclusions=None, search_term_preparer=None):
        self.config = Config.get_config()
        self.wikidata_service = WikidataService()
        self.stats = Stats()
        if wikidata_exclusions is None:
            self.wikidata_exclusions = self.config["TRANSLATION"]["wikidata-exclusions_default"]
        else:
            self.wikidata_exclusions = wikidata_exclusions
        if search_term_preparer is None:
            self.search_term_preparer = SearchTermPreparer(
                self.config["TRANSLATION"]["search-term-preparation_multiple-term-separator_default"],
                self.config["TRANSLATION"]["search-term-preparation_alternative-term-separator_default"]
            )
        else:
            self.search_term_preparer = search_term_preparer

    def __get_wikidata_labels__(self, term, languages):
        try:
            wikidata_result = self.wikidata_service.process_search(term, exclusions=self.wikidata_exclusions)
        except PermissionError as permission_error:
            raise permission_error
        except IOError:
            logging.exception("Unable to process translation")
            return {}
        wikidata_labels = {}
        for language in languages:
            for binding in wikidata_result:
                item = {}
                if "xml:lang" in binding["label"] and binding["label"]["xml:lang"] == language:
                    item["id"] = binding["item"]["value"].strip()
                    item["label"] = binding["label"]["value"].strip()
                    wikidata_labels[language] = item
                    break
        return wikidata_labels

    def __build_composite_label__(self, result_terms, languages):
        composite_labels = {}
        for language in languages:
            all_labels_for_language = []
            composite_labels_for_language = []
            for label in result_terms["multiTerm"]:
                composite_alternative_labels = []
                for alternative_label in label["altTerm"]:
                    if language in alternative_label["labels"]:
                        label = alternative_label["labels"][language]["label"]
                        if label not in all_labels_for_language:
                            composite_alternative_labels.append(label)
                            all_labels_for_language.append(label)
                if composite_alternative_labels:
                    composite_labels_for_language.append(" / ".join(composite_alternative_labels))
            composite_labels[language] = ", ".join(composite_labels_for_language)
        return composite_labels

    def __process_stats_update__(self, search_result, languages):
        self.stats.add_entry()
        for lng in languages:
            if search_result["compositeLabels"][lng]:
                self.stats.add_hit(lng)
            else:
                self.stats.add_miss(lng)

    def translate(self, term, languages):
        logging.debug("Starting translation of " + term)
        search = self.search_term_preparer.prepare(term)
        for multi_term in search["multiTerm"]:
            for alt_term in multi_term["altTerm"]:
                alt_term["labels"] = self.__get_wikidata_labels__(alt_term["term"], languages)
        search["compositeLabels"] = self.__build_composite_label__(search, languages)
        self.__process_stats_update__(search, languages)
        logging.debug(search)
        return search["compositeLabels"]

    def log_stats(self):
        logging.info("DURATION: %s, ENTRIES: %s, LANGUAGE_HITS: %s, PERCENTAGES: %s"
                     % (self.stats.elapsed_time(), self.stats.entries, self.stats.language_stats, dict(map(lambda l: (l[0], 100 * l[1] / self.stats.entries), self.stats.language_stats.items()))))


if __name__ == "__main__":
    Config.init_logging()
    translator = Translator()
    result = translator.translate(sys.argv[1], sys.argv[2].split(","))
    logging.info(result)
    translator.log_stats()
    print(result)
